/*
 * File: app/view/DealMainPanel.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealApp.view.DealMainPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dealmainpanel',

    requires: [
        'DealApp.view.DealMainPanelViewModel',
        'DealApp.view.NotesGridPanel',
        'DealApp.view.DocGridPanel',
        'DealApp.view.ContactGridPanel',
        'DealApp.view.InteractionGridPanel',
        'DealApp.view.DealGridPanel',
        'Ext.tab.Panel',
        'Ext.grid.Panel',
        'Ext.tab.Tab'
    ],

    viewModel: {
        type: 'dealmainpanel'
    },
    layout: 'border',
    title: 'Deals',

    items: [
        {
            xtype: 'tabpanel',
            region: 'south',
            split: true,
            frame: true,
            height: 300,
            id: 'southcontainer',
            collapsed: false,
            collapsible: true,
            header: false,
            activeTab: 0,
            items: [
                {
                    xtype: 'notesgridpanel'
                },
                {
                    xtype: 'docgridpanel'
                },
                {
                    xtype: 'contactgridpanel'
                },
                {
                    xtype: 'interactiongridpanel'
                },
                {
                    xtype: 'panel',
                    title: 'Users'
                },
                {
                    xtype: 'panel',
                    title: 'Tasks'
                },
                {
                    xtype: 'panel',
                    title: 'Loan History'
                }
            ]
        },
        {
            xtype: 'dealgridpanel',
            region: 'center'
        }
    ]

});