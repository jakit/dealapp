<?php   
	$dealarr = array();
	$notearr = array();
	$contactarr = array();
	$interactionsarr = array();
	
	$dealid = $_REQUEST['dealid'];
	if($dealid==1)
	{
		$dealarr[]['doc'] = array('dealid'=>1,'docid'=>1,'doctype'=>'pdf','doc_name'=>'dealstart.pdf','sz'=>"32 kbytes",'added'=>"Vish",'desc'=>"Descirption related to deal project",'createddate'=>"2014-11-11",'ctime'=>"10:50:00",'modifieddate'=>"2014-11-12",'modifiedtime'=>"12:00:00",'classification'=>'Pdf Document','modified'=>'Jak','associated'=>'Deal');
		$dealarr[]['doc'] = array('dealid'=>1,'docid'=>2,'doctype'=>'doc','doc_name'=>'deal.doc','sz'=>"40 kbytes",'added'=>"Vish",'desc'=>"Descirption related to project",'createddate'=>"2014-11-15",'ctime'=>"07:50:00",'modifieddate'=>"2014-11-14",'modifiedtime'=>"12:00:00",'classification'=>'Word Document','modified'=>'Jak','associated'=>'Property');
		$dealarr[]['doc'] = array('dealid'=>1,'docid'=>3,'doctype'=>'csv','doc_name'=>'deal.csv','sz'=>"20 kbytes",'added'=>"Vish",'desc'=>"Details about the project",'createddate'=>"2014-11-20",'ctime'=>"12:05:00",'modifieddate'=>"2014-11-15",'modifiedtime'=>"03:00:00",'classification'=>'CSV File','modified'=>'Jak','associated'=>'Deal');
		
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is about the deal Project.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"09:50:00",'associated'=>'Deal');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is very useful.','user'=>"Vish",'cdate'=>"2014-11-01",'ctime'=>"08:50:00",'associated'=>'Property');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'Deal price is very good.','user'=>"Vish",'cdate'=>"2014-11-23",'ctime'=>"12:50:00",'associated'=>'Deal');
		
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>1,'name'=>'Jak','company'=>"Apple",'contacttype'=>"Manager",'primaryphoneno'=>'9904362444','primaryemail'=>"jak@gmail.com",'associated'=>'Property');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>2,'name'=>'Demo','company'=>"Google",'contacttype'=>"Sales Executive",'primaryphoneno'=>'9904362444','primaryemail'=>"demo@gmail.com",'associated'=>'Deal');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>3,'name'=>'Shouki','company'=>"GM",'contacttype'=>"Dealer",'primaryphoneno'=>'9904362444','primaryemail'=>"shk@gmail.com",'associated'=>'Property');
		
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Jak','toname'=>"Apple",'desc'=>"Manager",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Demo','toname'=>"Google",'desc'=>"Sales Executive",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Shouki','toname'=>"GM",'desc'=>"Dealer",'idate'=>"2014-11-11");
	}
	else if($dealid==2)
	{
		$dealarr[]['doc'] = array('dealid'=>2,'docid'=>1,'doctype'=>'pdf','doc_name'=>'dealstart.pdf','sz'=>"32 kbytes",'added'=>"Vish",'desc'=>"Descirption related to deal project",'createddate'=>"2014-11-11",'ctime'=>"10:50:00",'modifieddate'=>"2014-11-12",'modifiedtime'=>"12:00:00",'classification'=>'Pdf Document','modified'=>'Jak','associated'=>'Property');
		$dealarr[]['doc'] = array('dealid'=>2,'docid'=>2,'doctype'=>'doc','doc_name'=>'deal.doc','sz'=>"40 kbytes",'added'=>"Vish",'desc'=>"Descirption related to project",'createddate'=>"2014-11-15",'ctime'=>"07:50:00",'modifieddate'=>"2014-11-14",'modifiedtime'=>"12:00:00",'classification'=>'Word Document','modified'=>'Jak','associated'=>'Deal');
		$dealarr[]['doc'] = array('dealid'=>2,'docid'=>3,'doctype'=>'csv','doc_name'=>'deal.csv','sz'=>"20 kbytes",'added'=>"Vish",'desc'=>"Details about the project",'createddate'=>"2014-11-20",'ctime'=>"12:05:00",'modifieddate'=>"2014-11-15",'modifiedtime'=>"03:00:00",'classification'=>'CSV File','modified'=>'Jak','associated'=>'Deal');
		
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is about the deal Project.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"07:50:00",'associated'=>'Deal');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is very useful.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"06:50:00",'associated'=>'Property');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'Deal price is very good.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"04:50:00",'associated'=>'Deal');
		
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>1,'name'=>'Jak','company'=>"Apple",'contacttype'=>"Manager",'primaryphoneno'=>'9904362444','primaryemail'=>"jak@gmail.com",'associated'=>'Property');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>2,'name'=>'Demo','company'=>"Google",'contacttype'=>"Sales Executive",'primaryphoneno'=>'9904362444','primaryemail'=>"demo@gmail.com",'associated'=>'Deal');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>3,'name'=>'Shouki','company'=>"GM",'contacttype'=>"Dealer",'primaryphoneno'=>'9904362444','primaryemail'=>"shk@gmail.com",'associated'=>'Property');
		
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Jak','toname'=>"Apple",'desc'=>"Manager",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Demo','toname'=>"Google",'desc'=>"Sales Executive",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Shouki','toname'=>"GM",'desc'=>"Dealer",'idate'=>"2014-11-11");
	}
	else if($dealid==3)
	{		
		$dealarr[]['doc'] = array('dealid'=>3,'docid'=>1,'doctype'=>'pdf','doc_name'=>'dealstart.pdf','sz'=>"32 kbytes",'added'=>"Vish",'desc'=>"Descirption related to deal project",'createddate'=>"2014-11-11",'ctime'=>"10:50:00",'modifieddate'=>"2014-11-12",'modifiedtime'=>"12:00:00",'classification'=>'Pdf Document','modified'=>'Jak','associated'=>'Deal');
		$dealarr[]['doc'] = array('dealid'=>3,'docid'=>2,'doctype'=>'doc','doc_name'=>'deal.doc','sz'=>"40 kbytes",'added'=>"Vish",'desc'=>"Descirption related to project",'createddate'=>"2014-11-15",'ctime'=>"07:50:00",'modifieddate'=>"2014-11-14",'modifiedtime'=>"12:00:00",'classification'=>'Word Document','modified'=>'Jak','associated'=>'Deal');
		$dealarr[]['doc'] = array('dealid'=>3,'docid'=>3,'doctype'=>'csv','doc_name'=>'deal.csv','sz'=>"20 kbytes",'added'=>"Vish",'desc'=>"Details about the project",'createddate'=>"2014-11-20",'ctime'=>"12:05:00",'modifieddate'=>"2014-11-15",'modifiedtime'=>"03:00:00",'classification'=>'CSV File','modified'=>'Jak','associated'=>'Property');
		
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is about the deal Project.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"10:50:00",'associated'=>'Property');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is very useful.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"09:50:00",'associated'=>'Deal');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'Deal price is very good.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"08:50:00",'associated'=>'Property');
		
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>1,'name'=>'Jak','company'=>"Apple",'contacttype'=>"Manager",'primaryphoneno'=>'9904362444','primaryemail'=>"jak@gmail.com",'associated'=>'Property');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>2,'name'=>'Demo','company'=>"Google",'contacttype'=>"Sales Executive",'primaryphoneno'=>'9904362444','primaryemail'=>"demo@gmail.com",'associated'=>'Deal');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>3,'name'=>'Shouki','company'=>"GM",'contacttype'=>"Dealer",'primaryphoneno'=>'9904362444','primaryemail'=>"shk@gmail.com",'associated'=>'Property');
		
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Jak','toname'=>"Apple",'desc'=>"Manager",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Demo','toname'=>"Google",'desc'=>"Sales Executive",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Shouki','toname'=>"GM",'desc'=>"Dealer",'idate'=>"2014-11-11");
	}
	else if($dealid==4)
	{
		$dealarr[]['doc'] = array('dealid'=>4,'docid'=>1,'doctype'=>'pdf','doc_name'=>'dealstart.pdf','sz'=>"32 kbytes",'added'=>"Vish",'desc'=>"Descirption related to deal project",'createddate'=>"2014-11-11",'ctime'=>"10:50:00",'modifieddate'=>"2014-11-12",'modifiedtime'=>"12:00:00",'classification'=>'Pdf Document','modified'=>'Jak','associated'=>'Property');
		$dealarr[]['doc'] = array('dealid'=>4,'docid'=>2,'doctype'=>'doc','doc_name'=>'deal.doc','sz'=>"40 kbytes",'added'=>"Vish",'desc'=>"Descirption related to project",'createddate'=>"2014-11-15",'ctime'=>"07:50:00",'modifieddate'=>"2014-11-14",'modifiedtime'=>"12:00:00",'classification'=>'Word Document','modified'=>'Jak','associated'=>'Deal');
		$dealarr[]['doc'] = array('dealid'=>4,'docid'=>3,'doctype'=>'csv','doc_name'=>'deal.csv','sz'=>"20 kbytes",'added'=>"Vish",'desc'=>"Details about the project",'createddate'=>"2014-11-20",'ctime'=>"12:05:00",'modifieddate'=>"2014-11-15",'modifiedtime'=>"03:00:00",'classification'=>'CSV File','modified'=>'Jak','associated'=>'Property');
		
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is about the deal Project.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"07:50:00",'associated'=>'Property');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'This note is very useful.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"05:50:00",'associated'=>'Deal');
		$notearr[]['note'] = array('dealid'=>1,'notetext'=>'Deal price is very good.','user'=>"Vish",'cdate'=>"2014-11-11",'ctime'=>"06:50:00",'associated'=>'Property');
		
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>1,'name'=>'Jak','company'=>"Apple",'contacttype'=>"Manager",'primaryphoneno'=>'9904362444','primaryemail'=>"jak@gmail.com",'associated'=>'Property');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>2,'name'=>'Demo','company'=>"Google",'contacttype'=>"Sales Executive",'primaryphoneno'=>'9904362444','primaryemail'=>"demo@gmail.com",'associated'=>'Deal');
		$contactarr[]['contact'] = array('dealid'=>1,'contactid'=>3,'name'=>'Shouki','company'=>"GM",'contacttype'=>"Dealer",'primaryphoneno'=>'9904362444','primaryemail'=>"shk@gmail.com",'associated'=>'Property');
		
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Jak','toname'=>"Apple",'desc'=>"Manager",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Demo','toname'=>"Google",'desc'=>"Sales Executive",'idate'=>"2014-11-11");
		$interactionsarr[]['interaction'] = array('dealid'=>1,'name'=>'Shouki','toname'=>"GM",'desc'=>"Dealer",'idate'=>"2014-11-11");
	}
	$aData = array('success' => true,'totaldocs'=>count($dealarr), 'docs'   => $dealarr,'totalnotes'=>count($notearr), 'notes'   => $notearr,'totalcontacts'=>count($contactarr), 'contacts'   => $contactarr,'totalinteractions'=>count($interactionsarr), 'interactions'   => $interactionsarr);
	echo json_encode($aData);
?>