# ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass/etc
    ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass/src
    ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass/var
