# ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-crisp-5f4ed4c4-f298-416e-8233-07e67b23c73c/sass/etc"`, these files
need to be used explicitly.
