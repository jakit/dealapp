# ext-theme-neptune-11ef19d7-034b-49d7-a848-fa5cf3520630/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-11ef19d7-034b-49d7-a848-fa5cf3520630/sass/etc"`, these files
need to be used explicitly.
